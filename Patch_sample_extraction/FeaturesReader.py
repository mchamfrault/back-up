from enum import Enum
import rasterio
import geopandas

class FeatureReadMode(Enum):
    ONE = 1
    ALL = 2

class PatchBounds:
    def __init__(self, col_min, col_max, row_min, row_max):
        self.col_min = col_min
        self.col_max = col_max
        self.row_min = row_min
        self.row_max = row_max

class FeaturesReader:
    def __init__(self, features_file, sample_selection_file, feature_read_mode: FeatureReadMode,
            radius_width, radius_height):
        self.features_file = features_file
        self.sample_selection_file = sample_selection_file
        self.feature_read_mode = feature_read_mode
        self.features_source = None
        self.samples = None
        self.features = None
        self.radius_width = radius_width
        self.radius_height = radius_height

    def open(self):
        self.samples = geopandas.GeoDataFrame.from_file(self.sample_selection_file)
        self.features_source = rasterio.open(self.features_file)
        if self.feature_read_mode is FeatureReadMode.ALL:
            self.features = self.features_source.read()

    def close(self):
        self.features_source.close()

    def get_samples(self):
        for index, sample in self.samples.iterrows():
            sample_idx = index + 1
            geometry = sample['geometry']
            yield sample_idx, geometry.x, geometry.y

    def get_patch_bounds(self, s_index, s_x, s_y):
        row, col = self.features_source.index(s_x, s_y)
        col_min = col - self.radius_width
        col_max = col + self.radius_width + 1
        row_min = row - self.radius_height
        row_max = row + self.radius_height + 1

        if col_min < 0 or col_max >= self.features_source.width or row_min < 0 or row_max >=  self.features_source.height:
            print(f"Skipping sample {s_index}, patch out of bounds")
            return False

        return PatchBounds(col_min, col_max, row_min, row_max)

    def get_features(self, patch_bounds: PatchBounds):
        if self.feature_read_mode is FeatureReadMode.ONE:
            self.features = self.features_source.read(window = ((patch_bounds.col_min, patch_bounds.col_max), (patch_bounds.row_min, patch_bounds.row_max)))

        for col in range(patch_bounds.col_min, patch_bounds.col_max):
            for row in range(patch_bounds.row_min, patch_bounds.row_max):
                f_row = row if self.feature_read_mode is FeatureReadMode.ALL else row - patch_bounds.row_min
                f_col = col if self.feature_read_mode is FeatureReadMode.ALL else col - patch_bounds.col_min
                yield [col - patch_bounds.col_min, row - patch_bounds.row_min, self.features[:, f_row, f_col]]
