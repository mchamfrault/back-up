import os
import shutil

def cutRasters(sourceDirectory, targetDirectory, perimeter):
    def extent(perimeter):
        import fiona
        with fiona.open(perimeter) as perimeterSource:
            bounds = perimeterSource.bounds
            window = [bounds[0], bounds[3], bounds[2], bounds[1]]
            return window, perimeterSource.crs['init']

    def cut(sourceFilePath, targetFilePath, perimeter):
        from osgeo import gdal
        # L93 : 2154 / WGS84 : 32631
        window, crs = extent(perimeter)
        source = gdal.Open(sourceFilePath)
        target = gdal.Warp(
            targetFilePath,
            source,
            outputBounds = window,
            outputBoundsSRS = crs)
        target = None

    print("sourceDirectory : " + sourceDirectory)
    print("targetDirectory : " + targetDirectory)

    if not os.path.isdir(targetDirectory):
        os.makedirs(targetDirectory)
    src = os.path.abspath(sourceDirectory)
    src_prefix = len(src) + len(os.path.sep)

    for sourceDirpath, sourceDirnames, sourceFilenames in os.walk(sourceDirectory):
        for sourceFilename in [f for f in sourceFilenames if f.endswith(".tif")]:
            """ sourceFilePath = os.path.join(sourceDirpath, sourceFilename)
            targetFilePath = os.path.join(targetDirectory, filename[len(to_remove):]) """
            sourceFilePath = os.path.join(sourceDirpath, sourceFilename)
            targetFilePath = os.path.join(targetDirectory, sourceDirpath[src_prefix:], sourceFilename)
            cut(sourceFilePath, targetFilePath, perimeter)
        for sourceDirname in sourceDirnames:
            targetDirectoryPath = os.path.join(targetDirectory, sourceDirpath[src_prefix:], sourceDirname)
            if not os.path.isdir(targetDirectoryPath):
                os.mkdir(targetDirectoryPath)

sourceDirectory = "/home/MChamfrault/Documents/DATAS/DL/sensor_data/T31TCJ"
targetDirectory = "/home/MChamfrault/Documents/DATAS/DL/sensor_data/T2"
emprise = "/home/MChamfrault/Documents/DATAS/DL/emprise2.shp"
cutRasters(sourceDirectory, targetDirectory, emprise)

def rename(rootDirectory):
    to_remove = "min_"
    for dirpath, dirnames, filenames in os.walk(rootDirectory):
        for filename in [f for f in filenames if f.startswith(to_remove)]:
            filePath = os.path.join(dirpath, filename)
            newFilePath = os.path.join(dirpath, filename[len(to_remove):])
            os.rename(filePath, newFilePath)

#rename("/home/MChamfrault/Documents/IOTA2_Minimal_TEST_S2/sensor_data")

def copyOrthoTiles(sourceDirectory, targetDirectory):
    print("sourceDirectory : " + sourceDirectory)
    print("targetDirectory : " + targetDirectory)
    
    for filename in os.listdir(sourceDirectory):
        if not filename.endswith(".jp2"):
            continue
        sourceTilePath =  os.path.join(sourceDirectory, filename)
        if not os.path.isfile(sourceTilePath):
            raise FileNotFoundError(sourceTilePath)
        parts = filename.split(".")[0].split("-", 2)
        tileName = parts[0] + "-" + parts[2]
        year = parts[1]
        targetTileDirectory = os.path.join(targetDirectory, tileName)
        if not os.path.isdir(targetTileDirectory):
            os.mkdir(targetTileDirectory)
        targetTileDirectory = os.path.join(targetTileDirectory, year)
        if not os.path.isdir(targetTileDirectory):
            os.mkdir(targetTileDirectory)
        shutil.copy2(sourceTilePath, targetTileDirectory)
        print("tile " + filename + " copied to " + targetTileDirectory)

""" sourceDirectories = ["/home/MChamfrault/Téléchargements/IOTA2_Dijon/ortho/BDORTHO_RVB-5M00_JP2-E100_RGF93LAMB93_D021_2014",
                    "/home/MChamfrault/Téléchargements/IOTA2_Dijon/ortho/BDORTHO_RVB-5M00_JP2-E100_RGF93LAMB93_D021_2017"]
targetDirectory = "/home/MChamfrault/Téléchargements/IOTA2_Dijon/ortho_data"

for sourceDirectory in sourceDirectories:
    copyOrthoTiles(sourceDirectory, targetDirectory) """
