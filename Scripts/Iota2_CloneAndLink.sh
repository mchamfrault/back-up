#!/bin/sh

. ./_Iota2_SetName.sh ${1}

CONDA_PATH="/home/MChamfrault/miniconda3/envs/${NAME}"
echo "CONDA_PATH=${CONDA_PATH}"
DEV_PATH="/home/MChamfrault/Documents/GIT/${NAME}"
echo "DEV_PATH=${DEV_PATH}"

mkdir $DEV_PATH
cd $DEV_PATH
git clone https://framagit.org/iota2-project/iota2.git
python ./iota2/utilities/enable_hooks.sh
echo "IOTA2DIR=${DEV_PATH}/iota2" > "${DEV_PATH}/iota2/.env"

cd "${CONDA_PATH}/lib/python3.9/site-packages"
mv iota2-0.0.0-py3.9.egg iota2-0.0.0-py3.9.egg_backup
ln -s "${DEV_PATH}/iota2/iota2" iota2-0.0.0-py3.9.egg

conda deactivate
conda activate $NAME
Iota2.py -h
