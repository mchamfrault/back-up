#!/bin/sh

if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit 1
fi

NAME="iota2-${1}"
echo "NAME=${NAME}"

# NEEDED to conda activate
# If launched with sh <thisScript>.sh
. ~/miniconda3/etc/profile.d/conda.sh
# If launched with bash <thisScript>.sh
# source ~/miniconda3/etc/profile.d/conda.sh

